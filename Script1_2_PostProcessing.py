# !/usr/local/bin/python

# THIS SCRIPT SHOWCASES THE USE OF THE 'ASTROML' PROGRAMMING FRAMEWORK
# FOR THE USE OF MACHINE LEARNING FOR PROPGRATION IN THE CR3BP FRAMEWORK.
# THIS SCRIPT EXEMPLIFIES HOW TO CREATE A STANDARD NEURAL NETWORK THAT,
# FOR PRESCRIBED VALUE OF THE JACOBIAN CONSTANT, TAKES AS INPUT THE
# STATE VECTOR OF A SPACECRAFT AT A GIVEN INSTANT OF TIME, AND RETURNS
# AS AN OUTPUT THE PREDICTED STATE VECTOR A TIMESTEP INTO THE FUTURE.


# Import modules
from astroml import *



# Load a previously trained model
Model = AstroML()
Model.Load ('Script 1_2 Trained Model')


# Make the Training/Validation and Input/Output data available, if needed.
# The original row indexing is preserved when shuffling, so it can be recovered if needed.
# The validation data was never shuffled, so it already preserves the original indexing.
TrainingInput    =  Model.Training.Input.sort_index()
TrainingOutput   =  Model.Training.Output.sort_index()
ValidationInput  =  Model.Validation.Input
ValidationOutput =  Model.Validation.Output


# We can plot the original 2D trajectory with which the NN was trained
ax = TrainingInput.plot (x ='x', y='y', kind = 'scatter', s=1)
ValidationInput.plot    (x ='x', y='y', kind = 'scatter', s=1, c='red', ax=ax)
plt.legend(['Training data', 'Validation data'])
plt.xlabel('x (adim)')
plt.ylabel('y (adim)')
plt.axis('equal')
plt.title('Original trajectory used to train NN')
plt.show()



# Now, we can simulate the outputs that the NN would provide for given inputs
# obtained from a dataset, be it the original dataset upon which the NN was 
# trained, or any other dataset that we would like to test. To that end:

# Make the desired dataset available. Let's do it first with the original dataset, so the
# original trajectory and the NN predicted trajectory can be compared upon one another
Data = Database()
Data.Load2D('DROexample')
Data.Vitamine()

# Export all data within the 'Database' object into a consolidated dataframe
Baseline = Data.Export()

# Simulate the outputs that the NN will predict upon the inputs from the provided database
Predicted = Model.Simulate(Data)

# Compare the baseline and predicted outputs
ax = Baseline.plot (x ='dx', y='dy', kind = 'scatter', s=2)
Predicted.plot     (x ='dx', y='dy', kind = 'scatter', s=1, c='red', ax=ax)
plt.legend(['Baseline data', 'Predicted data'])
plt.xlabel(r'$\Delta x$ (adim)')
plt.ylabel(r'$\Delta y$ (adim)')
plt.axis('equal')
plt.title('Output of NN vs Baseline Output')
plt.show()


# Additionally, we can try to use the NN model to predict a whole trajectory
# by recurrently using the NN model for successive timesteps, as follows:
PredictedTrajectory = Baseline.copy()
PredictedTrajectory.head(1)[Model.OutputVars] = Model.Predict( PredictedTrajectory.head(1)[Model.InputVars] )

for Step in range(Baseline.shape[0] - 1):
    x  = PredictedTrajectory.loc[Step, 'x']
    y  = PredictedTrajectory.loc[Step, 'y']
    vx = PredictedTrajectory.loc[Step, 'vx']
    vy = PredictedTrajectory.loc[Step, 'vy']
    dt = PredictedTrajectory.loc[Step, 'dt']
    
    Input = pd.DataFrame(data = [[x, y, vx, vy, dt]], columns = Model.InputVars)
    Output = Model.Predict(Input)
    
    PredictedTrajectory.at[Step+1, 'x']  = PredictedTrajectory.loc[Step, 'x']  + Output['dx']
    PredictedTrajectory.at[Step+1, 'y']  = PredictedTrajectory.loc[Step, 'y']  + Output['dy']
    PredictedTrajectory.at[Step+1, 'vx'] = PredictedTrajectory.loc[Step, 'vx'] + Output['dvx']
    PredictedTrajectory.at[Step+1, 'vy'] = PredictedTrajectory.loc[Step, 'vy'] + Output['dvy']

# Compare the baseline and predicted trajectories
ax = Baseline.plot       (x='x', y='y', kind = 'line')
PredictedTrajectory.plot (x='x', y='y', kind = 'line', ax=ax)
plt.legend(['Baseline Trajectory', 'Predicted Trajectory'])
plt.xlabel('x (adim)')
plt.ylabel('y (adim)')
plt.axis('equal')
plt.title('Baseline vs NN-Predicted Trajectory')
plt.show()


# Plot the error of the predicted trajectory wrt the baseline trajectory
B = Baseline[['x', 'y']].to_numpy()
P = PredictedTrajectory[['x', 'y']].to_numpy()
E = np.linalg.norm(P-B, axis=1)
t = Baseline['TimeStep'].to_numpy()

plt.plot(t, E)
plt.xlabel('Time (adim)')
plt.ylabel(r'$|E|$')
plt.title('Error Timeseries of NN-Predicted Trajectory')
plt.show()

# Plot the accumulated error of the predicted trajectory wrt the baseline trajectory
E_accum = np.cumsum(E)
plt.plot(t, E_accum)
plt.xlabel('Time (adim)')
plt.ylabel(r'$|E|$')
plt.title('Accumulated Error Timeseries of NN-Predicted Trajectory')
plt.show()
