# !/usr/local/bin/python

# THIS SCRIPT SHOWCASES THE USE OF THE 'ASTROML' PROGRAMMING FRAMEWORK
# FOR THE USE OF MACHINE LEARNING FOR PROPGRATION IN THE CR3BP FRAMEWORK.
# THIS SCRIPT EXEMPLIFIES HOW TO CREATE A STANDARD NEURAL NETWORK THAT,
# FOR PRESCRIBED VALUE OF THE JACOBIAN CONSTANT, TAKES AS INPUT THE
# STATE VECTOR OF A SPACECRAFT AT A GIVEN INSTANT OF TIME, AND RETURNS
# AS AN OUTPUT THE PREDICTED STATE VECTOR A TIMESTEP INTO THE FUTURE.


# Import modules
from astroml import *


# Create an instance of a 'Database' class
Data = Database()

# Load the csv files containing the database
Data.Load2D('DROexample')



# We can now extend the database by adding complementary, derived information
# to it into additional columns. To this end, note custom-made methods can be
# implemented into the 'Database' class as needed.
Data.Vitamine()

# Split data into "training" and "validation" subsets
TrainingData, ValidationData = Data.Split (TakeEvery = 5)


# Create an instance of an 'AstroML' class
Model = AstroML()

# Define Inputs and Outputs for the Model

Model.Type = "lstm"

if Model.Type == "lstm":
	Model.window = 3
	Model.InputVars  = ["x", "y", "vx", "vy"]
	Model.OutputVars = ["x", "y", "vx", "vy"]
	TrainingData = TrainingData[Model.InputVars]
	ValidationData = ValidationData[Model.OutputVars]
	

elif Model.Type == "standard":

	Model.InputVars  = ["x", "y", "vx", "vy", "dt"]
	Model.OutputVars = ["dx", "dy", "dvx", "dvy"]

Model.Training = TrainingData
Model.Validation = ValidationData

# Provide the training and validation data and do additional pre-processing

Model.Set     (Model.Training, Model.Validation, Type = Model.Type)


# Create the Neural Network Model. Default values can be overriden
Model.CreateNN (Type = Model.Type, Layer_Density = 400, Learning_Rate = 0.0001)

# Start training of the model
Model.Train (Batch_Size = 40, Epochs = 100, Validation_Split = 0.25)

# Evaluate the losses
Model.Evaluate ()

# Monitor training success and accuracy of the model after training
Model.Monitor ()

# Save the model for later use without needed to train it again
Model.Save ('Script 2 Trained Model')


# ------------------------------------------------------------


# A previously saved, already trained model can also be loaded:
NewModel = AstroML()
NewModel.Load ('Script 2 Trained Model')

NewModel.Evaluate ()

