# !/usr/local/bin/python

# THIS SCRIPT SHOWCASES THE USE OF THE 'ASTROML' PROGRAMMING FRAMEWORK
# FOR THE USE OF MACHINE LEARNING FOR PROPGRATION IN THE CR3BP FRAMEWORK.
# THIS SCRIPT EXEMPLIFIES HOW TO CREATE A STANDARD NEURAL NETWORK THAT,
# FOR PRESCRIBED INITIAL CONDITIONS, TAKES AS INPUT A TIMESTEP, AND
# RETURNS AS AN OUTPUT THE PREDICTED STATE VECTOR AT THAT TIMESTEP.


# Import modules
from astroml import *



# Create an instance of a 'Database' class
Data = Database()

# Load the csv files containing the database
Data.Load2D('fullTrajectories/1')

# Split data into "training" and "validation" subsets
TrainingData, ValidationData = Data.Split (TakeEvery = 5)



# Create an instance of an 'AstroML' class
Model = AstroML()

# Define Inputs and Outputs for the Model
Model.InputVars  = ["TimeStep"]
Model.OutputVars = ["x", "y", "vx", "vy"]

# Provide the training and validation data and do additional pre-processing
Model.Set     (TrainingData, ValidationData)
Model.Scale   ()
Model.Shuffle ()

# Create the Neural Network Model. Default values can be overriden
Model.CreateNN (Type = 'standard', Layer_Density = 300, Learning_Rate = 0.001)

# Start training of the model
#Model.Train (Batch_Size = 40, Epochs = 6000, Validation_Split = 0.25)
Model.Train (Batch_Size = 40, Epochs = 3, Validation_Split = 0.25)

# Evaluate the losses
Model.Evaluate ()

# Monitor training success and accuracy of the model after training
Model.Monitor ()

# Save the model for later use without needed to train it again
Model.Save ('Script 1 Trained Model')


# ------------------------------------------------------------


# A previously saved, already trained model can also be loaded:
NewModel = AstroML()
NewModel.Load ('Script 1 Trained Model')

NewModel.Evaluate ()

