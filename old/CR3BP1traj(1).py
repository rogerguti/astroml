import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import os
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D

from keras.layers import Dense, Input, concatenate
from keras.models import Model
from sklearn.preprocessing import StandardScaler
from keras.regularizers import l2
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from sklearn.utils import shuffle


# creamos el modelo
def create_model(data_par1, input_train_1):
    model = tf.keras.Sequential([
        tf.keras.layers.Input(shape=(1,)),
        # tf.keras.layers.Input(shape=(9,)),
        tf.keras.layers.Dense(300, activation='sigmoid', name='dense01'),
        tf.keras.layers.Dense(300, activation='sigmoid', name='dense02'),             
        tf.keras.layers.Dense(300, activation='sigmoid', name='dense03'),
        tf.keras.layers.Dense(300, activation='sigmoid', name='dense04'),
        
        tf.keras.layers.Dense(6, name='Output_Layer')
    ])

    learning_rate = 0.001
    model.compile(optimizer=tf.keras.optimizers.Adamax(learning_rate),
                  loss='mae', metrics=['accuracy', ])
    model.summary()
    return model


def calculate_error(outcome_test, p_test):
    error = []
    for outs in zip(outcome_test, p_test):
        error.append((outs[0] - outs[1])**2)
    return error


def error_relativo(error, outcome_test):
    error_rel = np.sqrt(error) / np.absolute(outcome_test)
    return error_rel

def drop_constant_columns(dataframe):
    """
    Drops constant value columns of pandas dataframe.
    """
    for column in dataframe.columns:
        if len(dataframe[column].unique()) == 1:
            dataframe.drop(column,inplace=True,axis=1)
    return dataframe


def main():

    filename_par1 = 'par_1_HighEnergy_Primary.csv'
    filename_traj1 = 'traj_1_HighEnergy_Primary.csv'

    data_par1 = pd.read_csv(filename_par1, names=["mu","Jacobi constant","Prop. time","x[0]","y[0]","z[0]","vx[0]","vy[0]","vz[0]"])
    data_traj1 = pd.read_csv(filename_traj1, names=["TimeStep","x","y","z","vx","vy","vz"])

    print(data_traj1)

    data = pd.DataFrame({"TimeStep": [], "x": [], "y": [], "z": [],"vx": [],"vy": [],"vz": []})
    data = data_traj1.iloc[0:100000:2]

    print("DATA", data.shape)

    data_test_1 = data.iloc[0:50000:5]
    data_train_1 = data.drop(data_test_1.index)

    data_par = pd.DataFrame({"mu": [], "Jacobi constant": [], "Prop. time": [], "x[0]": [
        ], "y[0]": [], "z[0]": [], "vx[0]": [], "vy[0]": [], "vz[0]": []})

    output_train = data_train_1[["x","y","z","vx","vy","vz"]]
    input_train_1 = data_train_1[["TimeStep"]]

    for i in range(1000):
        data_par = data_par.append(data_par1)

    data_par_11 = pd.DataFrame({"mu": [], "Jacobi constant": [], "Prop. time": [], "x[0]": [
        ], "y[0]": [], "z[0]": [], "vx[0]": [], "vy[0]": [], "vz[0]": []})

    for i in range(50):
        data_par_11 = data_par_11.append(data_par)

    data_par_11 = data_par_11.reset_index(drop=True)

    input_test_11 = data_par_11.iloc[0:50000:5]
    input_train_11 = data_par_11.drop(input_test_11.index)

    output_test = data_test_1[["x","y","z","vx","vy","vz"]]
    input_test_1 = data_test_1[["TimeStep"]]

    input_train_1 = input_train_1.reset_index(drop=True)
    input_test_1 = input_test_1.reset_index(drop=True)

    input_train_11 = input_train_11.reset_index(drop=True)
    input_test_11 = input_test_11.reset_index(drop=True)

    output_train = output_train.reset_index(drop=True)
    output_test = output_test.reset_index(drop=True)

    input_train_11.loc[:, 'TimeStep'] = input_train_1
    input_test_11.loc[:, 'TimeStep'] = input_test_1

    print("INPUT TRAIN 1", input_train_11)
    print("OUTPUT TRAIN ", output_train)

    print("INPUT TEST 1", input_test_11)
    print("OUTPUT TEST ", output_test)


    input_train_11 = drop_constant_columns(input_train_11)
    input_test_11 = drop_constant_columns(input_test_11)

    print("INPUT TRAIN 1", input_train_11)

    scaler = MinMaxScaler(feature_range=(0, 1))
    # Fit on training set only.
    scaler.fit(output_train)

    # Apply transform to both the training set and the test set.
    output_train = scaler.transform(output_train)
    output_test = scaler.transform(output_test)

    print("OUTPUT TRAIN ESCALADO",output_train)
    print("OUTPUT TEST ESCALADO",output_test)

    input_train_11, output_train = shuffle(input_train_11, output_train)
    print(input_train_11, output_train)

    model = create_model(data_par1,input_train_1)

    history = model.fit(x=input_train_11, y=output_train,
                                    batch_size=40, epochs=6000, validation_split=0.25, verbose=1)

    # evaluamos las perdidas
    loss, accuracy = model.evaluate(x=input_test_11, y=output_test, verbose=2)
    print('Loss:', loss, 'Accuracy:', accuracy)

    # --------MONITOR
    # Plot training & validation loss values
    fig3 = plt.figure(figsize=(9, 6))
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validate'], loc='upper left')
    plt.show()

    # Plot training & validation accuracy values
    fig3 = plt.figure(figsize=(9, 6))
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validate'], loc='upper left')
    plt.show()

    # predecimos con los punto de test
    p_test = model.predict(input_test_11)
    print(p_test.shape)
    x_val = [x[0] for x in output_test]
    y_val = [x[1] for x in output_test]
    z_val = [x[2] for x in output_test]
    vx_val = [x[3] for x in output_test]
    vy_val = [x[4] for x in output_test]
    vz_val = [x[5] for x in output_test]

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(x_val , y_val, z_val)
    ax.plot(p_test[:, 0], p_test[:, 1], p_test[:, 2])
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(x_val )
    plt.plot(p_test[:, 0])
    plt.xlabel('Time Steps')
    plt.ylabel('X coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(y_val)
    plt.plot(p_test[:, 1])
    plt.xlabel('Time Steps')
    plt.ylabel('Y coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(z_val)
    plt.plot(p_test[:, 2])
    plt.xlabel('Time Steps')
    plt.ylabel('Z coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(vx_val )
    plt.plot(p_test[:, 3])
    plt.xlabel('Time Steps')
    plt.ylabel('X velocity')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(vy_val)
    plt.plot(p_test[:, 4])
    plt.xlabel('Time Steps')
    plt.ylabel('Y velocity')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(vz_val)
    plt.plot(p_test[:, 5])
    plt.xlabel('Time Steps')
    plt.ylabel('Z velocity')
    plt.show()

if __name__ == '__main__':
    main()
