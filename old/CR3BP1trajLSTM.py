import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import os
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D

from keras.layers import Dense, Input, concatenate
from keras.models import Model
from sklearn.preprocessing import StandardScaler
from keras.regularizers import l2
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from sklearn.utils import shuffle


# creamos el modelo
def create_model(window):
    model = tf.keras.Sequential([
        tf.keras.layers.LSTM(
            units=300, return_sequences=True, input_shape=(window, 6)),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.LSTM(units=300),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(6, name='Output_Layer')
    ])

    learning_rate = 0.0001
    model.compile(optimizer=tf.keras.optimizers.Adamax(learning_rate),
                  loss='mae', metrics=['accuracy', ])
    model.summary()
    return model


def calculate_error(outcome_test, p_test):
    error = []
    for outs in zip(outcome_test, p_test):
        error.append((outs[0] - outs[1])**2)
    return error


def error_relativo(error, outcome_test):
    error_rel = np.sqrt(error) / np.absolute(outcome_test)
    return error_rel


def drop_constant_columns(dataframe):
    """
    Drops constant value columns of pandas dataframe.
    """
    for column in dataframe.columns:
        if len(dataframe[column].unique()) == 1:
            dataframe.drop(column, inplace=True, axis=1)
    return dataframe


def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return np.array(dataX), np.array(dataY)


def main():

    filename_par1 = 'par_1_HighEnergy_Primary.csv'
    filename_traj1 = 'traj_1_HighEnergy_Primary.csv'

    data_par1 = pd.read_csv(filename_par1, names=[
                            "mu", "Jacobi constant", "Prop. time", "x[0]", "y[0]", "z[0]", "vx[0]", "vy[0]", "vz[0]"])
    data_traj1 = pd.read_csv(filename_traj1, names=[
                             "TimeStep", "x", "y", "z", "vx", "vy", "vz"])

    print(data_traj1)

    data = pd.DataFrame({"TimeStep": [], "x": [], "y": [],
                         "z": [], "vx": [], "vy": [], "vz": []})
    data = data_traj1.iloc[0:100000:2]

    print("DATA", data)

    data_test_1 = data.iloc[0:50000:5]
    data_train_1 = data.drop(data_test_1.index)

    data_par = pd.DataFrame({"mu": [], "Jacobi constant": [], "Prop. time": [], "x[0]": [
    ], "y[0]": [], "z[0]": [], "vx[0]": [], "vy[0]": [], "vz[0]": []})

    output_train = data_train_1[["x", "y", "z", "vx", "vy", "vz"]]
    input_train_1 = data_train_1[["TimeStep"]]

    for i in range(1000):
        data_par = data_par.append(data_par1)

    data_par_11 = pd.DataFrame({"mu": [], "Jacobi constant": [], "Prop. time": [], "x[0]": [
    ], "y[0]": [], "z[0]": [], "vx[0]": [], "vy[0]": [], "vz[0]": []})

    for i in range(50):
        data_par_11 = data_par_11.append(data_par)

    data_par_11 = data_par_11.reset_index(drop=True)

    input_test_11 = data_par_11.iloc[0:50000:5]
    input_train_11 = data_par_11.drop(input_test_11.index)

    

    output_test = data_test_1[["x", "y", "z", "vx", "vy", "vz"]]
    input_test_1 = data_test_1[["TimeStep"]]

    input_train_1 = input_train_1.reset_index(drop=True)
    input_test_1 = input_test_1.reset_index(drop=True)

    input_train_11 = input_train_11.reset_index(drop=True)
    input_test_11 = input_test_11.reset_index(drop=True)

    output_train = output_train.reset_index(drop=True)
    output_test = output_test.reset_index(drop=True)

    scaler = MinMaxScaler(feature_range=(0, 1))
    # Fit on training set only.
    scaler.fit(output_train[["x","y","z","vx","vy","vz"]])

    # Apply transform to both the training set and the test set.
    output_train[["x","y","z","vx","vy","vz"]] = scaler.transform(output_train[["x","y","z","vx","vy","vz"]])
    output_test[["x","y","z","vx","vy","vz"]] = scaler.transform(output_test[["x","y","z","vx","vy","vz"]])

    output_train = np.asarray(output_train)
    print(output_train)

    output_test = np.asarray(output_test)
    print(output_test)

    window = 4

    yin = []
    next_y1 = []
    for i in range(window, len(output_train)):
        yin.append(output_train[i-window:i])
        next_y1.append(output_train[i])

    yin, next_y1 = np.array(yin), np.array(next_y1)
    yin = yin.reshape(yin.shape[0], yin.shape[1], 6)

    print("YIN", yin)
    print("NEXT Y1 ", next_y1)

    print("INPUT TRAIN 1", input_train_1)
    print("OUTPUT TRAIN ", output_train)

    print("INPUT TEST 1", input_test_1)
    print("OUTPUT TEST ", output_test)

    input_train_11.loc[:, 'TimeStep'] = input_train_1
    input_test_11.loc[:, 'TimeStep'] = input_test_1

    print("INPUT TRAIN 1", input_train_11)
    print("OUTPUT TRAIN ", output_train)

    print("INPUT TEST 1", input_test_11)
    print("OUTPUT TEST ", output_test)


    model = create_model(window)

    history = model.fit(x=yin, y=next_y1, 
                        batch_size=50,
                        epochs=300, 
                        validation_split=0.25, 
                        verbose=1)

    # evaluamos las perdidas
    loss, accuracy = model.evaluate(x=yin, y=next_y1, verbose=2)
    print('Loss:', loss, 'Accuracy:', accuracy)


    # --------MONITOR
    # Plot training & validation loss values
    fig3 = plt.figure(figsize=(9, 6))
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validate'], loc='upper left')
    plt.show()

    # Plot training & validation accuracy values
    fig3 = plt.figure(figsize=(9, 6))
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validate'], loc='upper left')
    plt.show()

    yin = []
    next_y2 = []
    for i in range(window, len(output_test)):
        yin.append(output_test[i-window:i])
        next_y2.append(output_test[i])

    yin, next_y2 = np.array(yin), np.array(next_y2)
    yin = yin.reshape(yin.shape[0], yin.shape[1], 6)

    print(yin)

    # predecimos con los punto de test
    p_test = model.predict(yin)

    p_test = scaler.inverse_transform(p_test)
    next_y2 = scaler.inverse_transform(next_y2) 

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(next_y2[:, 0], next_y2[:, 1], next_y2[:, 2])
    ax.plot(p_test[:, 0], p_test[:, 1], p_test[:, 2])
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(next_y2[:, 0])
    plt.plot(p_test[:, 0])
    plt.xlabel('Time Steps')
    plt.ylabel('X coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(next_y2[:, 1])
    plt.plot(p_test[:, 1])
    plt.xlabel('Time Steps')
    plt.ylabel('Y coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(next_y2[:, 2])
    plt.plot(p_test[:, 2])
    plt.xlabel('Time Steps')
    plt.ylabel('Z coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(next_y2[:, 3])
    plt.plot(p_test[:, 3])
    plt.xlabel('Time Steps')
    plt.ylabel('vx')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(next_y2[:, 4])
    plt.plot(p_test[:, 4])
    plt.xlabel('Time Steps')
    plt.ylabel('vy')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(next_y2[:, 5])
    plt.plot(p_test[:, 5])
    plt.xlabel('Time Steps')
    plt.ylabel('vz')
    plt.show()


if __name__ == '__main__':
    main()
