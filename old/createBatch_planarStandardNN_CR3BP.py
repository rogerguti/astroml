# Machine Learning Algorithms for the CR3BP

## Docstring
__author__      = 'TBD'
__affiliation__ = 'TBD'
__email__       = 'TBD'
__date__        = '30 March 2021'
__version__     = '0.0.1' 
__status__      = 'Development'
__copyright__   = 'TBD' 
__license__     = 'TBD' 

## Imports and method declarations
# Import general packages and modules
import sys

#%matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import pickle
import os
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
plt.style.use('seaborn-darkgrid')

import cProfile
from pstats import Stats, SortKey

from keras.layers import Dense, Input, concatenate
from keras.models import Model
from sklearn.preprocessing import StandardScaler
from keras.regularizers import l2
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from sklearn.utils import shuffle

# Import personal packages and modules
from support.functions import * # pylint: disable=C0301
from support.variables import CR3BPParameters
#import support.variables_3bp
import support.functions_3bp as tbp

## Machine Learning Algorithm
def standard_NN_training(dataDirectory, modelsDirectory, currentDatabase):

    filenamePar = currentDatabase + '/parameters'
    filenameTraj = currentDatabase + '/trajectories'

    # Load the csv databases
    print('Load the csv datafiles')
    parData, trajData = load_data(filenamePar, filenameTraj, dataDirectory)
    #print(parData)
    #print(trajData)
    print('Done')

    # Data preprocessing
    print('Data preprocessing trajectory')
    trajTestData, trajTrainData = standradNN_data_preprocessing(trajData, 'planarTrajectory')
    print('Data preprocessing parameters')
    parTestData, parTrainData = standradNN_data_preprocessing(parData, 'planarParameters')
    
    #print(trajTestData.shape)
    #print(trajTrainData.shape)  

    trainInput, testInput, trainOutput, testOutput = standradNN_inputOutputData_preprocessing(trajTrainData, trajTestData)
    print('Done')

    print('Add timesteps to parameters datafram')
    parTestData = add_timesteps_to_parameters_dataframe(parTestData, testInput)
    parTrainData = add_timesteps_to_parameters_dataframe(parTrainData, trainInput)
    print('Done')    
    # Print final input/output frames of train and test data

    print("INPUT TRAIN PARAMETERS \n", parTrainData)
    #print("OUTPUT TRAIN \n ", trainOutput)

    print("INPUT TEST PARAMETERS \n", parTestData)
    #print("OUTPUT TEST \n ", testOutput)

    # Drop constant columns of the parameters + timestep dataframes: everything except the timesteps...
    print('Drop constant columns')
    parTrainData = drop_constant_columns(parTrainData)
    parTestData = drop_constant_columns(parTestData)
    print('Done')
    print("INPUT TRAIN PARAMETERS \n", parTrainData)
    print("INPUT TEST PARAMETERS \n", parTestData)

    print('Create the scaler and transform training and test set')
    # Create the scaler operator with range 0-1
    scaler = MinMaxScaler(feature_range=(0, 1))
    # Fit on training set only.
    scaler.fit(trainOutput)

    # Apply transform to both the training set and the test set.
    trainOutput = scaler.transform(trainOutput)
    testOutput = scaler.transform(testOutput)
    print('Done')
    #print("SCALED OUTPUT TRAIN \n", trainOutput)
    #print("SCALED OUTPUT TEST \n", testOutput)

    print('Shuffle the datasets')
    # Shuffle the training sets in a random but consistent way
    parTrainData, trainOutput = shuffle(parTrainData, trainOutput)
    print('Done')
    #print(parTrainData, trainOutput)

    print('Create the model')
    # Create the model
    model = create_model('standard',300,0.001,'planar')
    print('Done')

    print('Start training of the model')
    # Actual training of the model.
    history = model.fit(x=parTrainData, y=trainOutput, batch_size=40, epochs=6000, validation_split=0.25, verbose=1)
    print('Done')

    # Evaluate the losses.
    loss, accuracy = model.evaluate(x=parTestData, y=testOutput, verbose=2)
    print('Loss:', loss, 'Accuracy:', accuracy)

    print('Save everything')
    save_model_variables(modelsDirectory, currentDatabase, model, parData, trajData, parTrainData, parTestData, trainOutput, testOutput)
    print('Done')
    
    print('Plot the results')
 
    # --------MONITOR
    # Plot training & validation loss values
    fig3 = plt.figure(figsize=(9, 6))
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validate'], loc='upper left')
    plt.show()

    # Plot training & validation accuracy values
    fig3 = plt.figure(figsize=(9, 6))
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validate'], loc='upper left')
    plt.show()

    # predecimos con los punto de test
    p_test = model.predict(parTestData)
    print(p_test.shape)
    x_val = [x[0] for x in testOutput]
    y_val = [x[1] for x in testOutput]
    vx_val = [x[2] for x in testOutput]
    vy_val = [x[3] for x in testOutput]

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(x_val , y_val)
    ax.plot(p_test[:, 0], p_test[:, 1])
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(x_val )
    plt.plot(p_test[:, 0])
    plt.xlabel('Time Steps')
    plt.ylabel('X coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(y_val)
    plt.plot(p_test[:, 1])
    plt.xlabel('Time Steps')
    plt.ylabel('Y coordinate')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(vx_val )
    plt.plot(p_test[:, 2])
    plt.xlabel('Time Steps')
    plt.ylabel('X velocity')
    plt.show()

    fig5 = plt.figure(figsize=(9, 6))
    plt.plot(vy_val)
    plt.plot(p_test[:, 3])
    plt.xlabel('Time Steps')
    plt.ylabel('Y velocity')
    plt.show() 

    print('Script finsihed')

if __name__ == '__main__':

    # Specify directory directories from where to load/save data
    # Data directory
    dataDirectory = 'data/fullTrajectories/'
    # Model directory 
    modelsDirectory = 'savedModels/fullTrajectories/'

    for i in range(1):
        standard_NN_training(dataDirectory, modelsDirectory, str(i+1))
