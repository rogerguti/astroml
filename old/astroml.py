# Machine Learning Algorithms for the CR3BP

# Docstring
__author__      = 'TBD'
__affiliation__ = 'TBD'
__email__       = 'TBD'
__date__        = '11 September 2021'
__version__     = '0.0.1' 
__status__      = 'Development'
__copyright__   = 'TBD' 
__license__     = 'TBD' 


# Import general packages and modules
import sys
import os
import pickle


# Import numerical packages and modules
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle

from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
plt.style.use('seaborn-darkgrid')


# Import Machine Learning packages and modules
from keras.layers import Dense, Input, concatenate
from keras.models import Model, load_model
from keras.regularizers import l2





class Database:

    # Raw data as a pandas dataframe
    class Trajectory:
        Parameters = pd.DataFrame()
        TimeSeries = pd.DataFrame()
        Additional = pd.DataFrame()
    

    Directory  = 'data/'    # Data directory
    Dimension  = []         # Dimensionality of the data: 2 for planar motion, 3 for three-dimensional
    
    Raw = Trajectory()


    # Load the 2D data from the specified folder contatining the data set
    def Load2D(self, Folder):

        self.Dimension = 2

        filename_params = self.Directory + Folder + '/parameters.csv'
        filename_series = self.Directory + Folder + '/trajectories.csv'

        self.Raw.Parameters = pd.read_csv(filename_params, sep=',', names=["mu", "Jacobi constant", "Prop. time", "x[0]", "y[0]", "vx[0]", "vy[0]"])
        self.Raw.TimeSeries = pd.read_csv(filename_series, sep=',', names=["TimeStep", "x", "y", "vx", "vy"])


    # Load the 3D data from the specified folder contatining the data set
    def Load3D(self, Folder):

        self.Dimension = 3

        filename_params = self.Directory + Folder + '/parameters.csv'
        filename_series = self.Directory + Folder + '/trajectories.csv'

        self.Raw.Parameters = pd.read_csv(filename_params, sep=',', names=["mu", "Jacobi constant", "Prop. time", "x[0]", "y[0]", "z[0]", "vx[0]", "vy[0]", "vz[0]"])
        self.Raw.TimeSeries = pd.read_csv(filename_series, sep=',', names=["TimeStep", "x", "y", "z", "vx", "vy", "vz"])


    # Extends the current database by adding further columns that contain additional, derived information of the
    # original dataset. In particular, we shall add new columns that provide information of the time increment
    # and the change of Cartesian coordinates from the current timestep to the next one. Therefore, we shall
    # drop the last row (i.e. the last entry) of the database, since these increments cannot be computed.
    def Vitamine(self):

        # Convert dataframe to numeric array
        array = self.Raw.TimeSeries.to_numpy()

        # Create another numeric array to store the increments from one row to the next
        delta = np.zeros(shape = self.Raw.TimeSeries.shape)

        for row in range(self.Raw.TimeSeries.shape[0] - 1):
            delta[row] = array[row+1] - array[row]
        
        # Create a new dataframe for the derived information
        if self.Dimension == 2:
            self.Raw.Additional = pd.DataFrame(data = delta[:-1], columns = ["dt", "dx", "dy", "dvx","dvy"])
        
        elif self.Dimension == 3:
            self.Raw.Additional = pd.DataFrame(data = delta[:-1], columns = ["dt", "dx", "dy", "dz", "dvx","dvy", "dvz"])

        # For consistency, drop the last entry (i.e. last row) of the TimeSeries dataframe
        self.Raw.TimeSeries.drop(self.Raw.TimeSeries.tail(1).index, inplace=True)


    # Splits the data into 2 subsets: 'training data' and 'test data', and gives them the correct formattings.
    # In: 'TakeEvery' is the validation split, i.e. the validation subset is set as 1 per every 'TakeEvery' entries
    # Out: Returns TrainingData & TestData dataframes
    def Split(self, TakeEvery = 5):
        
        # Join TimeSeries and Additional (if any) data into a combined dataframe
        if self.Raw.Additional.empty:
            Joined = self.Raw.TimeSeries
        else:
            Joined = pd.concat([self.Raw.TimeSeries, self.Raw.Additional], axis=1)

        # Create 'validation data' subset: cut the data by taking 1 every 'TakeEvery' values
        Len = Joined.shape[0]
        ValidationData = Joined.iloc[0 : Len : TakeEvery]

        # Create 'training data' subset: drop test data from the original data
        TrainingData = Joined.drop(ValidationData.index)

        # Reset indices of all trainnig/test arrays
        TrainingData   = TrainingData.reset_index(drop=True)
        ValidationData = ValidationData.reset_index(drop=True)
        
        # Expand the training and test data subsets to include the parameters
        TrainingAux    = pd.concat([self.Raw.Parameters] * TrainingData.shape[0], ignore_index=True)
        TrainingData   = pd.concat([TrainingAux, TrainingData], axis=1)

        ValidationAux  = pd.concat([self.Raw.Parameters] * ValidationData.shape[0], ignore_index=True)
        ValidationData = pd.concat([ValidationAux, ValidationData], axis=1)
        
        return TrainingData, ValidationData



class AstroML:
    
    class Subset:
        Input  = pd.DataFrame()
        Output = pd.DataFrame()
    

    Directory = 'model/'    # Model directory

    InputVars  = []         # Specifies the labels of the dataset entries that will be the input  of the model
    OutputVars = []         # Specifies the labels of the dataset entries that will be the output of the model
    Dimension  = 2          # Dimensionality of the problem: 2 for planar problems, 3 for 3D
    
    Training   = Subset()   # Training   data subset containing the Input and Output sets
    Validation = Subset()   # Validation data subset containing the Input and Output sets

    NN = []                 # Neural Network Model
    Training_History = []   # Training History. Useful to monitor stats


    # Create the input/output sets for the 'training' and 'test' subsets and do additional pre-processing
    def Set (self, TrainingData, ValidationData):
        
        # Slice training and validation data to extract 'Input' data for the model
        self.Training.Input    = TrainingData   [self.InputVars]
        self.Validation.Input  = ValidationData [self.InputVars]

        # Slice training and validation data to extract 'Output' data for the model
        self.Training.Output   = TrainingData   [self.OutputVars]
        self.Validation.Output = ValidationData [self.OutputVars]

    
    # Create the scaler operator and transform the output training and validation subsets
    def Scale (self, scaling = 'MinMaxScaler'):
        
        # Create the scaler operator with range 0-1
        if scaling == 'MinMaxScaler':
            scaler = MinMaxScaler(feature_range=(0, 1))

        # Fit on training set only
        scaler.fit(self.Training.Output)

        # Apply transform to both the training set and the test set.
        self.Training.Output   = pd.DataFrame (data = scaler.transform(self.Training.Output),   columns = self.Training.Output.columns  )
        self.Validation.Output = pd.DataFrame (data = scaler.transform(self.Validation.Output), columns = self.Validation.Output.columns)
    

    # Shuffle the training input/output sets in a random but consistent way
    def Shuffle (self):
        self.Training.Input, self.Training.Output = shuffle(self.Training.Input, self.Training.Output)


    # Create the Neural Network Model
    def CreateNN (self, Type = 'standard', Layer_Density = 300, Learning_Rate = 0.001):

        if Type == 'standard':
            self.NN = tf.keras.Sequential([
                tf.keras.layers.Input (shape = (len(self.InputVars),)),
                tf.keras.layers.Dense (Layer_Density, activation='sigmoid', name='dense01'),
                tf.keras.layers.Dense (Layer_Density, activation='sigmoid', name='dense02'),             
                tf.keras.layers.Dense (Layer_Density, activation='sigmoid', name='dense03'),
                tf.keras.layers.Dense (Layer_Density, activation='sigmoid', name='dense04'),
                tf.keras.layers.Dense (Layer_Density, activation='sigmoid', name='dense05'),
                tf.keras.layers.Dense (2*self.Dimension, name='Output_Layer')
            ])

        elif Type == 'lstm':
            window = 4
            self.NN = tf.keras.Sequential([
                tf.keras.layers.LSTM    (units=Layer_Density, return_sequences=True, input_shape=(window, 2*self.Dimension)),
                tf.keras.layers.Dropout (0.2),
                tf.keras.layers.LSTM    (units=Layer_Density),
                tf.keras.layers.Dropout (0.2),
                tf.keras.layers.Dense   (2*self.Dimension, name='Output_Layer')
            ])

        self.NN.compile (optimizer = tf.keras.optimizers.Adamax(Learning_Rate),
                       loss = 'mae',
                       metrics = ['accuracy', ])
                  
        self.NN.summary()
    

    # Train the Neural Network Model
    def Train (self, Batch_Size = 40, Epochs = 6000, Validation_Split = 0.25):
        History = self.NN.fit (
            x = self.Training.Input,
            y = self.Training.Output,
            batch_size = Batch_Size,
            epochs = Epochs,
            validation_split = Validation_Split,
            verbose=1
        )

        self.Training_History = History.history
    

    # Evaluate the losses
    def Evaluate (self):
        
        loss, accuracy = self.NN.evaluate (x = self.Validation.Input, y = self.Validation.Output, verbose = 0)
        print('\n', 'Loss:', loss, '    Accuracy:', accuracy, '\n')

        return loss, accuracy
    

    # Monitor training success and accuracy of the model after training
    def Monitor (self):
        
        # Plot training & validation loss values
        fig3 = plt.figure (figsize=(9, 6))
        plt.plot (self.Training_History['loss'])
        plt.plot (self.Training_History['val_loss'])
        plt.title  ('Model loss')
        plt.ylabel ('Loss')
        plt.xlabel ('Epoch')
        plt.legend (['Train', 'Validate'], loc='upper left')
        plt.show()

        # Plot training & validation accuracy values
        fig3 = plt.figure (figsize=(9, 6))
        plt.plot (self.Training_History['accuracy'])
        plt.plot (self.Training_History['val_accuracy'])
        plt.title  ('Model accuracy')
        plt.ylabel ('Accuracy')
        plt.xlabel ('Epoch')
        plt.legend (['Train', 'Validate'], loc='upper left')
        plt.show()

        # We predict upon the validation points
        p_test = self.NN.predict (self.Validation.Input)
        print(self.Validation.Output)
        x_val  = self.Validation.Output[['x']]
        y_val  = self.Validation.Output[['y']]
        vx_val = self.Validation.Output[['vx']]
        vy_val = self.Validation.Output[['vy']]

        x_array = x_val.to_numpy().flatten()
        y_array = y_val.to_numpy().flatten()
        vx_array = vx_val.to_numpy().flatten()
        vy_array = vy_val.to_numpy().flatten()
        print(x_array)

        fig = plt.figure()
        #ax  = fig.gca
        plt.plot (x_array, y_array)
        plt.plot (p_test[:, 0], p_test[:, 1])
        plt.ylabel ('dy')
        plt.xlabel ('dx')
        plt.legend (['Original', 'NN'], loc='upper left')
        plt.show()

        fig5 = plt.figure (figsize=(9, 6))
        plt.plot (x_val )
        plt.plot (p_test[:, 0])
        plt.xlabel ('Time Steps')
        plt.ylabel ('X coordinate')
        plt.show()

        fig5 = plt.figure (figsize=(9, 6))
        plt.plot (y_val)
        plt.plot (p_test[:, 1])
        plt.xlabel ('Time Steps')
        plt.ylabel ('Y coordinate')
        plt.show()

        fig5 = plt.figure (figsize=(9, 6))
        plt.plot (vx_val )
        plt.plot (p_test[:, 2])
        plt.xlabel ('Time Steps')
        plt.ylabel ('X velocity')
        plt.show()

        fig5 = plt.figure (figsize=(9, 6))
        plt.plot (vy_val)
        plt.plot (p_test[:, 3])
        plt.xlabel ('Time Steps')
        plt.ylabel ('Y velocity')
        plt.show()
    

    # Save to a file
    def Save (self, filename):

        if not os.path.exists(self.Directory + filename):
            os.makedirs(self.Directory + filename)

        self.NN.save(self.Directory + filename + '/NN.h5')

        with open(self.Directory + filename + '/Obj.pkl', 'wb') as file:
            pickle.dump([
                self.Directory, 
                self.InputVars,
                self.OutputVars,
                self.Dimension,
                self.Training,
                self.Validation,
                self.Training_History], file)
        
        print('Model <', filename + '> was successfully saved.')
    

    # Load previously saved model from a file
    def Load (self, filename):

        self.NN = load_model (self.Directory + filename + '/NN.h5')

        with open(self.Directory + filename + '/Obj.pkl', 'rb') as file:
            self.Directory, self.InputVars, self.OutputVars, self.Dimension, self.Training, self.Validation, self.Training_History = pickle.load(file)
        
        print('Model <', filename + '> was successfully loaded.')

