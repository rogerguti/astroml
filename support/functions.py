#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Groups all astroML support functions.

    Module that contains all the necessary functions for the astroML
    execution.
"""

__author__      = 'TBD' # pylint: disable=C0326
__affiliation__ = 'TBD' # pylint: disable=C0326
__email__       = 'TBD' # pylint: disable=C0326
__date__        = '30 March 2021' # pylint: disable=C0326
__version__     = '0.0.1' # pylint: disable=C0326
__status__      = 'Development' # pylint: disable=C0326
__copyright__   = 'TBD' # pylint: disable=C0326
__license__     = 'TBD' # pylint: disable=C0326

import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import pickle
import os
from scipy.integrate import odeint, solve_ivp
from mpl_toolkits.mplot3d import Axes3D

from keras.layers import Dense, Input, concatenate
from keras.models import Model
from sklearn.preprocessing import StandardScaler
from keras.regularizers import l2
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from sklearn.utils import shuffle


# Function to load the data and assign it to pandas Dataframe
def load_data(param, trajectory, dataDirectory):
    # Load the data:
    filename_par = dataDirectory + param + '.csv'
    filename_traj = dataDirectory + trajectory + '.csv'

    data_par = pd.read_csv(filename_par, names=["mu","Jacobi constant","Prop. time","x[0]","y[0]","vx[0]","vy[0]"])
    data_traj = pd.read_csv(filename_traj, names=["TimeStep","x","y","vx","vy"])
    
    return data_par, data_traj

# Data prepping
def standradNN_data_preprocessing(rawData, dataType):
    """Bundle of preprocessing routines for the raw trajectory or parameters datasets.

    Creates the training data and the test data, and gives them the correct formattings.

    Args:
      rawData: Raw input pandas dataframe

    Returns:
      testData: Test data processed and ready to use.
      trainData: Train data processed and ready to use.
    """
    
    if dataType == 'planarTrajectory':
        # Create trajectory Dataframe
        data = pd.DataFrame({"TimeStep": [], "x": [], "y": [], "vx": [],"vy": []})

        # Cut the data in half by taking only even rows
        data = dataSlicing(rawData, rawData.shape[0], 2)
        testTrainDataLength = rawData.shape[0]/2

    elif dataType == 'planarParameters':
        # Create Parameters Dataframe
        data = pd.DataFrame({"mu": [], "Jacobi constant": [], "Prop. time": [], "x[0]": [], "y[0]": [], "vx[0]": [], "vy[0]": []})
        data = rawData

        #TODO: This I think should'nt be hardcoded, but i want to go fast rn
        testTrainDataLength = 50000
        print('Data appending')
        # Creating a 50000 row (half of trajectory length) copy of parameterData...
        data = pd.concat([data]*int(testTrainDataLength), ignore_index=True)

    elif dataType == '3dTrajectory':
        #TODO: Not checked, just quick copypaste and adapt
        # Create trajectory Dataframe
        data = pd.DataFrame({"TimeStep": [], "x": [], "y": [], "z": [], "vx": [],"vy": [], "vz": []})

        # Cut the data in half by taking only even rows
        data = dataSlicing(rawData, rawData.shape[0], 2)
        testTrainDataLength = rawData.shape[0]/2

    elif dataType == '3dParameters':
        #TODO: Not checked, just quick copypaste and adapt
        # Create Parameters Dataframe
        data = pd.DataFrame({"mu": [], "Jacobi constant": [], "Prop. time": [], "x[0]": [], "y[0]": [], "z[0]": [], "vx[0]": [], "vy[0]": [], "vz[0]": []})
    
        #TODO: This I think should'nt be hardcoded, but i want to go fast rn
        testTrainDataLength = 50000
        # Creating a 50000 row (half of trajectory length) copy of parameterData...
        for i in range(int(testTrainDataLength)):
            data = data.append(rawData)

        # Add proper index
        data = data.reset_index(drop=True)       

    # Create test data: cut the data by taking every 5 values
    testData = dataSlicing(data, testTrainDataLength, 5)

    # Drop the test data from original data to create train data
    trainData = data.drop(testData.index)

    # Reset indices of all train/test arrays
    trainData = trainData.reset_index(drop=True)
    testData = testData.reset_index(drop=True)

    return testData, trainData

# Data prepping
def standradNN_inputOutputData_preprocessing(trainData, testData):
    """Create the input/output test and train dataframes

    Args:
      testData: test dataframe
      trainData: train dataframe

    Returns:
      trainInput: input train data.
      testInput: input test data.
      trainOutput: output train data.
      testOutput: output test data.
    """

    # Create train and test data frame without state vector
    trainInput = trainData[["TimeStep"]]
    testInput = testData[["TimeStep"]]

    # Create train and test data frame without timestep
    trainOutput = trainData[["x","y","vx","vy"]]
    testOutput = testData[["x","y","vx","vy"]]


    #TODO: I think this is not necessary now, as I reset everything when I create the test/train data in previous function
    # Reset indices of all train/test arrays
    #trainInput = trainInput.reset_index(drop=True)
    #testInput = testInput.reset_index(drop=True)
    #trainOutput = trainOutput.reset_index(drop=True)
    #testOutput = testOutput.reset_index(drop=True)

    return trainInput, testInput, trainOutput, testOutput

# Slice data for different purposes
def dataSlicing(inputData, length, takeEvery):
    return inputData.iloc[0:int(length):takeEvery]

# Adds the timesteps to the parameters dataframes...
def add_timesteps_to_parameters_dataframe(parameterDataframe, trajectoryDataframe):
        
    parameterDataframe.loc[:, 'TimeStep'] = trajectoryDataframe

    return parameterDataframe

# Create the model
def create_model(NN_type, layer_density, learning_rate, orbit_type):
    """Creates the model to train.

    Args:
      NN_type: Type of NN to train. Currently: 'standard' or 'lstm'
      layer_density: Density for the layer.
      learning_rate: Learning rate for the algorithm.
      orbit_type: Type of orbit data. 'planar' or '3d'

    Returns:
      model: The model.
    """

    if orbit_type == 'planar':
        state_vector_length = 4
    elif orbit_type == '3d':
        state_vector_length = 6

    if NN_type == 'standard':
        model = tf.keras.Sequential([
            tf.keras.layers.Input(shape=(1,)),
            # tf.keras.layers.Input(shape=(9,)),
            tf.keras.layers.Dense(layer_density, activation='sigmoid', name='dense01'),
            tf.keras.layers.Dense(layer_density, activation='sigmoid', name='dense02'),             
            tf.keras.layers.Dense(layer_density, activation='sigmoid', name='dense03'),
            tf.keras.layers.Dense(layer_density, activation='sigmoid', name='dense04'),
            
            tf.keras.layers.Dense(state_vector_length, name='Output_Layer')
        ])

    elif NN_type == 'lstm':
        window = 4
        model = tf.keras.Sequential([
            tf.keras.layers.LSTM(
                units=layer_density, return_sequences=True, input_shape=(window, state_vector_length)),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.LSTM(units=layer_density),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(state_vector_length, name='Output_Layer')
        ])

    model.compile(optimizer=tf.keras.optimizers.Adamax(learning_rate),
                  loss='mae', metrics=['accuracy', ])
                  
    model.summary()
    
    return model


def calculate_error(outcome_test, p_test):
    """Calculates the error of the result.

    Args:
      outcome_test: TBD
      p_test: TBD

    Returns:
      error: Error of the result.
    """
    
    error = []
    
    for outs in zip(outcome_test, p_test):
        error.append((outs[0] - outs[1])**2)
        
    return error


def calculate_relative_error(error, outcome_test):
    """Calculates the relative error of the result.

    Args:
      error: TBD
      outcome_test: TBD

    Returns:
      error_rel: Relative error of the result.
    """
    
    error_rel = np.sqrt(error) / np.absolute(outcome_test)
    
    return error_rel


def drop_constant_columns(dataframe):
    """Drops constant value columns of pandas dataframe.

    Args:
      dataframe: Initial dataframe.

    Returns:
      dataframe: Dataframe without the constant value columns.
    """

    for column in dataframe.columns:
        if len(dataframe[column].unique()) == 1:
            dataframe.drop(column,inplace=True,axis=1)
            
    return dataframe

def save_model_variables(modelsDirectory, filename, model, parData, trajData, parTrainData, parTestData, trainOutput, testOutput):

    modelDirectory = modelsDirectory + filename + '/'
    # Save the model
    modelName = modelDirectory + 'model'
    model.save(modelName)

    # We save important variables to evaluate the model
    # Order is always, data_par1, data_traj1, input_train_11, input_test_11, output_train, output_test
    # Open a file and use dump()
    with open(modelDirectory + 'trainingVariables.pkl', 'wb') as file:
      
        # A new file will be created
        pickle.dump([parData, trajData, parTrainData, parTestData, trainOutput, testOutput], file)


#TODO: Check for LSTM
def create_dataset(dataset, look_back=1):
    """TBD.

    Args:
      dataset: TBD
      look_back: TBD

    Returns:
      TBD: TBD
    """

    dataX, dataY = [], []

    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
        
    return np.array(dataX), np.array(dataY)


def propagator_cr3bp(cr3bp_parameters):
    """Propagator for the ODE of the circular restricted 3 body problem.

    Args:
      cr3bp_parameters: CR3BPParameters class with the parameters of the CR3BP trajectory.

    Returns:
      cr3bp_trajectory: Propagated state vector for the object.
    """

    print(
        'Solving the ODE for the Lunar far away swing-by with values: \n'
        'Initial conditions: ', cr3bp_parameters.initial_conditions, '\n mu: ',
        cr3bp_parameters.mu, '\n Time Span: ', cr3bp_parameters.time_span,
        '\n Time Step: ', cr3bp_parameters.time_step)

    # Solve the ODE for the CR3BP trajectory
    cr3bp_trajectory_sol = integrate.solve_ivp(
        state_vector_cr3bp,
        cr3bp_parameters.time_span,
        cr3bp_parameters.initial_conditions,
        method='RK45',
        t_eval=cr3bp_parameters.time_points,
        rtol=1e-12,
        atol=1e-12,
        args=(cr3bp_parameters.mu, ))

    print('ODE Solved!\n')

    # Unpacking of variables from ode
    cr3bp_trajectory = np.array([
        cr3bp_trajectory_sol.y[0], cr3bp_trajectory_sol.y[1],
        cr3bp_trajectory_sol.y[2], cr3bp_trajectory_sol.y[3],
        cr3bp_trajectory_sol.y[4], cr3bp_trajectory_sol.y[5]
    ])

    return cr3bp_trajectory


def state_vector_cr3bp(time, state_vector, mu):  # pylint: disable=C0103,W0613
    """ODE for the circular restricted 3 body problem to numerically integrate.

    Args:
      state_vector: State vector for the objects.
      time: Time vector with the time steps where the ode needs to integrate.
      mu: Mass parameter of the CR3BP.

    Returns:
      delta_state_vector: Time derivative of the state vector that needs to be integrated.
    """

    delta_state_vector = [0 for i in range(6)]

    distance_primary = np.sqrt((mu + state_vector[0])**2 + state_vector[1]**2 +
                               state_vector[2])
    distance_secondary = np.sqrt((state_vector[0] - 1 + mu)**2 +
                                 state_vector[1]**2 + state_vector[2]**2)

    # Spacecraft position in rotating frame
    delta_state_vector[0] = state_vector[3]
    delta_state_vector[1] = state_vector[4]
    delta_state_vector[2] = state_vector[5]

    # Spacecraft velocity
    delta_state_vector[3] = 2 * state_vector[4] + state_vector[0] - (
        1 - mu) * (state_vector[0] + mu) / distance_primary**3 - mu * (
            state_vector[0] - 1 + mu) / distance_secondary**3
    delta_state_vector[4] = -2 * state_vector[3] + state_vector[1] - (
        1 - mu) * state_vector[1] / distance_primary**3 - mu * state_vector[
            1] / distance_secondary**3
    delta_state_vector[5] = -state_vector[2] + state_vector[2] - (
        1 - mu) * state_vector[2] / distance_primary**3 - mu * state_vector[
            2] / distance_secondary**3

    return delta_state_vector


def lorenz_2d(time, state_vector, sigma, beta, rho):  # pylint: disable=W0613
    """ODE for the Lorenz attractor to numerically integrate.

    Args:
        state_vector: State vector for the objects.
        time: Time vector with the time steps where the ode needs to integrate.
        sigma: Lorenz's parameter
        beta: Lorenz's parameter
        rho: Lorenz's parameter

    Returns:
        delta_state_vector: Time derivative of the state vector that needs to be integrated.
    """

    delta_state_vector = [0 for i in range(3)]

    # ODE
    delta_state_vector[0] = sigma * (state_vector[1] - state_vector[0])
    delta_state_vector[1] = state_vector[0] * (
        rho - state_vector[2]) - state_vector[1]
    delta_state_vector[
        2] = state_vector[0] * state_vector[1] - beta * state_vector[2]

    return delta_state_vector
