#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Groups all astroML support variables.

    Module that contains all the necessary variables for the astroML
    execution.
"""

__author__      = 'Roger Gutierrez Ramon'  # pylint: disable=C0326
__affiliation__ = 'Institute of Space and Astronautical Science, ' \
                  'Japan Aerospace Exploration Agency' # pylint: disable=C0326
__email__       = 'roger.gutierrez@ac.jaxa.jp'  # pylint: disable=C0326
__date__        = '31 March 2020'  # pylint: disable=C0326
__version__     = '0.0.1'  # pylint: disable=C0326
__status__      = 'Development'  # pylint: disable=C0326
__copyright__   = 'Copyright 2020, Roger Gutierrez Ramon'  # pylint: disable=C0326
__license__     = 'TBD'  # pylint: disable=C0326

import sys

from dataclasses import dataclass
import math as m
import numpy as np


@dataclass
class CR3BPParameters:
    """Dataclass that contains the parameters of a CR3BP trajectory.

    Attributes:
        initial_conditions: A numpy array with the initial conditions of the
                            CR3BP trajectory (x,y,z,u,v,w).
        mu: Mass parameter of the CR3BP.
        time_span = Time span during which to propagate the CR3BP
        time_step = Time step for each propagation point.
        time_points = Time points where the trajectory will be calculated.
    """

    initial_conditions: float
    mu: float  # pylint: disable=C0103
    time_span: float
    time_step: float
    time_points: float = 0

    def __post_init__(self):
        self.time_points = np.linspace(
            self.time_span[0], self.time_span[1],
            int(self.time_span[1] / self.time_span[0]))
