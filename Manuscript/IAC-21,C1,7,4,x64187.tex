\documentclass[]{iac}
% To make the list of abbreviations and symbols. 
\usepackage[acronym]{glossaries}
\makeglossaries
\include{abbreviations}
\usepackage{mathptmx}
\usepackage{cite}

% Extra Roger packages
\usepackage{physics}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage{tabulary}
\usepackage{booktabs}
\usepackage{hyperref}

\newcommand{\etalia}{\textit{et al.}}
\newcommand*{\vectornorm}[1]{\left\|#1\right\|}
\newcommand*\rfrac[2]{{{}^{#1}\!/_{#2}}} % running fraction with slash - requires math mode.
\newcommand*\T{\mathsf{T}}

\begin{document}

\IACpaperyear{21}
\IACpapernumber{C1,7,4,x64187}
\IACconference{72}
\IAClocation{Dubai, United Arab Emirates, 25-29 October 2021.}
\IACcopyrightA{2021}{International Astronautical Federation (IAF)}

\title{Assessment of Hybrid Machine Learning Applications on Trajectories in the CR3BP}

\IACauthor{Roger Gutierrez-Ramon$^*$}{Department of Space and Astronautical Science, The Graduate University for Advanced Studies, SOKENDAI, Institute of Space and Astronautical Science (ISAS) 3-1-1 Yoshinodai Chuo-ku, Sagamihara City, Kanagawa Prefecture, Japan 252-5210, \underline{roger.gutierrez@ac.jaxa.jp}}{,}
\IACauthor{Pablo Solano-López}{Departamento/Universidad, Direccion de la URJC, \underline{pablo.solano@urjc.es}}{,} 
\IACauthor{Hodei Urrutxua}{Departamento/Universidad, Direccion de la URJC, \underline{hodei.urrutxua@urjc.es}}{,} 
%include the ^* next to the Main Author/ {, } Include a comma if it is NOT the last Author
\IACauthor{Adrian Barea}{Departamento/Universidad, Direccion de la URJC, \underline{adrian.barea@urjc.es}}{.} 
%{.} Include a dot if it IS the last Author

\abstract{Classical dynamical systems theory has been the basis for the study of dynamical systems since its development. However, in recent years many scientific and engineering areas, including the study of astrodynamics, have turned their attention to the application of new methodologies. In the analysis of the Circular Restricted 3 Body Problem (CR3BP), traditionally the study of periodic orbits, stability regions and invariant manifolds has been used to design trajectories and study their characteristics. These dynamical structures are obtained using numeral algorithms relying on iterative techniques and differential correction methods. In this paper, we investigate the applicability of novel methodologies based on Machine Learning to solve equivalent problems. More specifically, we focus on state-of-the-art hybrid techniques of classical mathematical models and Machine Learning, evaluating their performance and applicability to the astrodynamics domain. We design, train and benchmark different Neural Network (NN) schemes to predict the behaviour of CR3BP trajectories. Amongst the NN that we study are reservoir-like NN, namely an Echo State Network (ESN), and a Recurrent Neural Network (RNN), in particular the Long Short-Term Memory (LSTM) scheme, with different configurations of nodes and inner layers. These NN are suitable candidates for solving astrodynamical problems, as they are designed precisely to deal with time series and time-dependent functions. They also exhibit good robustness properties and predictive capability when learning complex data or modeling problems with complex dynamics, thus being used in applications with a high number of variables and data. For the NN’s training procedure, we feed the NN with a subset of numerically computed synthetic, fully non-linear trajectories with predetermined initial and boundary conditions. Sample trajectories with different energy levels and characteristics are used to avoid overfitting of the NN and help them capture the full behaviour of the CR3BP, while a different subset of trajectories is used for validation purposes. Results obtained from the NN are used to compare the feasibility of their use as a standalone tool for astrodynamical studies. The accurateness of the results in the past, as well as the Lyapunov times are used for evaluation. The studies include the tailoring of different hybrid schemes that combine the NN with numerical propagators, alternative training methods and possible pitfalls with respect to classical techniques, including numerical burden and accuracy. Benchmark trajectories include typical scenarios in the Earth-Moon vicinity as well as other dynamical systems of the Solar System.}
\IACkeywords{Neural Networks (NN)}{Circular Restricted 3 Body Problem (CR3BP)}{Hybrid Schemes}{Benchmark}{}{}

\maketitle
% Add list of symbols 
%\printglossary[title=Nomenclature]

\section*{Nomenclature}
This section is not numbered. A nomenclature section could be provided when there are mathematical symbols in your paper. Superscripts and subscripts must be listed separately. Nomenclature definitions should not appear again in the text.


%\section*{Acronyms/Abbreviations}
\printglossary[type=\acronymtype , title=Abbreviations]
This section is not numbered. Define acronyms and abbreviations that are not standard in this section. Such acronyms and abbreviations that are unavoidable in the abstract must be defined at their first mention there. Ensure consistency of abbreviations throughout the article. Always use the full title followed by the acronym (abbreviation) to be used, e.g., reusable suborbital launch vehicle (RSLV), International Space Station (ISS).
%\vskip 1.5ex%


\section{Introduction}
Classical dynamical systems theory has been the basis for the study of dynamical systems since its development. However, in recent years many scientific and engineering areas, including the study of astrodynamics, have turned their attention to the application of new methodologies. In the analysis of the \gls{CR3BP}, traditionally the study of periodic orbits, stability regions and invariant manifolds has been used to design trajectories and study their characteristics. These dynamical structures are obtained using numeral algorithms relying on iterative techniques and differential correction methods. In this paper, we investigate the applicability of novel methodologies based on \gls{ML} to solve equivalent problems. More specifically, we focus on state-of-the-art hybrid techniques of classical mathematical models and Machine Learning, evaluating their performance and applicability to the astrodynamics domain. We design, train and benchmark different \gls{NN} schemes to predict the behaviour of \acrshort{CR3BP} trajectories. Amongst the \gls{NN} that we study are reservoir-like \gls{NN}, namely an \gls{ESN}, and a \gls{RNN}, in particular the \gls{LSTM} scheme, with different configurations of nodes and inner layers. These \gls{NN} are suitable candidates for solving astrodynamical problems, as they are designed precisely to deal with time series and time-dependent functions. They also exhibit good robustness properties and predictive capability when learning complex data or modeling problems with complex dynamics, thus being used in applications with a high number of variables and data. For the \gls{NN}’s training procedure, we feed the \gls{NN} with a subset of numerically computed synthetic, fully non-linear trajectories with predetermined initial and boundary conditions. Sample trajectories with different energy levels and characteristics are used to avoid overfitting of the \gls{NN} and help them capture the full behaviour of the \gls{CR3BP}, while a different subset of trajectories is used for validation purposes. Results obtained from the \gls{NN} are used to compare the feasibility of their use as a standalone tool for astrodynamical studies. The accurateness of the results in the past, as well as the Lyapunov times are used for evaluation. The studies include the tailoring of different hybrid schemes that combine the \gls{NN} with numerical propagators, alternative training methods and possible pitfalls with respect to classical techniques, including numerical burden and accuracy. Benchmark trajectories include typical scenarios in the Earth-Moon vicinity as well as other dynamical systems of the Solar System.

\section{Reference Trajectories Generation}

Reference trajectories are needed for the training of the \glspl{NN}. This section details the framework used to obtain these trajectories and the procedures used. The benchmark trajectories used to evaluate the results obtained from the different \gls{NN} schemes are obtained in the same framework, from different initial conditions.

\subsection{The Circular Restricted 3 Body Problem}
REWORK EVERYTHING
\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\columnwidth]{Figures/cr3bp}
	\caption{The Planar Restricted Three-Body Problem.} 
	\label{FIG_cr3bp}
\end{figure}

The basis of this study is the \gls{PCR3BP}. In the \gls{PCR3BP}, a massless particle moves in the gravity field of two massive bodies, which revolve around their barycentre in a circular motion. This is a good abstraction of a spacecraft moving in the Earth-Moon system, where the spacecraft's mass is negligible compared to the celestial bodies. To ease in the computations, in the \gls{PCR3BP}, the total mass of the Earth and Moon are normalized to 1. The median distance between the two bodies is taken as the reference for the circular motion and normalized as 1, while the Moon's period is normalized to $2\pi$. The rotating frame defined has the origin at the barycenter of the system, with the $x$-axis at the Earth-Moon line and positive with the same direction, while the $y$-axis is positive in the direction of the Moon's velocity. The rotation period of this system is defined equal to the period of the Moon, so that both bodies remain on the $x$-axis. For the \gls{PCR3BP}, we use the mass ratio of the system, defined as $\mu = \frac{m_{\text{Moon}}}{m_{\text{Earth}} + m_{\text{Moon}}}$. In this system, the coordinates of the Earth are $(-\mu,0)$, and the coordinates of the Moon $(1-\mu,0)$, as shown in Fig.~\ref{FIG_cr3bp}. The coordinates of the spacecraft in this system are $(x, y)$, and the equations of motion are written as \begin{equation}
	\begin{aligned}
		\ddot{x} - 2 \dot{y} &= \Omega_x \, ,\\
		\ddot{y} + 2 \dot{x} &= \Omega_y \, ,\\ 
	\end{aligned}
	\label{EQ_eom}
\end{equation}
with the pseudo-potential
\begin{equation}
	\Omega(x,y) = \frac{1}{2} \left( x^2 + y^2 \right) + \frac{1 - \mu}{r_1} + \frac{\mu}{r_2} + \frac{\mu(1-\mu)}{2}\, .
	\label{EQ_eom_omega}
\end{equation}

The subscripts of $\Omega$ in Eq.~\eqref{EQ_eom} denote the partial derivatives with respect to the coordinates of the spacecraft, while $r_1$ and $r_2$ in Eq.~\eqref{EQ_eom_omega} denote the distances from the spacecraft to the Earth and Moon (respectively) as
\begin{equation}
	\begin{aligned}
		r_1 = \sqrt{\left( \mu + x \right)^2 + y^2} \, ,\\
		r_2 = \sqrt{\left(1 - \mu - x\right)^2 + y^2} \, .\\ 
	\end{aligned}
	\label{EQ_eom_adimdistances}
\end{equation}

This system has a first integral of motion, the Jacobi Integral, along with its constant of integration, the Jacobi Constant, defined as
\begin{equation}
	C = x^2 + y^2 + \frac{2\left(1 - \mu \right)}{r_1} + \frac{2 \mu}{r_2} + \mu(1-\mu) - \dot{x}^2 - \dot{y}^2 \, .
	\label{EQ_eom_jacobi}
\end{equation}

\begin{table}[htbp]
%	\fontsize{10}{10}\selectfont
	\caption{Parameters of the Earth-Moon System used in this research. Retrieved from SPICE\cite{Acton2018, Folkner2014}.}
	\label{TAB_systemparameters}
	\centering 
	\begin{tabular}{@{}l  c @{}} % Column formatting, 
		\toprule
		Parameter    & Value \\
		\midrule 
		Mass ratio      & $0.0121506$  \\
		Characteristic Length & $384402$ km  \\
		Characteristic Time & $27.284819$ days \\
		Characteristic Velocity & $1.0245442$ km/s \\
		$L_1$ admin. coordinates & $(0.836915, 0)$    \\
		$L_2$ admin. coordinates & $(1.155682, 0)$    \\
		$L_3$ admin. coordinates & $(-1.005063, 0)$   \\
		$L_4$ admin. coordinates & $(0.487849, 0.866025)$   \\
		$L_5$ admin. coordinates & $(0.487849, 0.866025)$ \\
		\bottomrule
	\end{tabular}
\end{table}

The \gls{PCR3BP} has five equilibrium points (also known as Lagrange points). The three collinear points $L_{1,2,3}$ are located along the $x$-axis, while the triangular points $L_{4,5}$ are located at the vertex of two equilateral triangles having the Earth-Moon distance as a common base. Table~\ref{TAB_systemparameters} lists the parameters for the Earth-Moon used in this research.

\subsection{Trajectory Design Algorithms}

Two sub-sets of trajectories are generated to be used as training datasets.
\begin{itemize}
	\item Open trajectories naturally drifting around in the \gls{CR3BP}. By changing the energy level of the particle (i.e. Jacobi Constant), different initial conditions are obtained, which are propagated for different time durations. Dense trajectories are obtained in the whole state space without any other controlling algorithm.
	\item Periodic Orbit families. These orbits are closed and periodic, generated on the \gls{PCR3BP} with the application of Differential Correction algorithms (for singular periodic orbits) and Numerical Continuation algorithms (to obtain families out of a singular orbit). One period of each orbit of the family is used for training. These algorithms are summarized in the next section, while the full derivation can be found in \cite{GutierrezRamon2020a}.
\end{itemize} 

\subsubsection{Differential Correction}

A Single Shooting Differential Correction algorithm used to generate periodic orbits in the \gls{CR3BP}. We define the state vector of the spacecraft as $\mathbf{X}$ (equivalent to Eq.~\eqref{EQ_eom}), and its time derivative as $\dot{\mathbf{X}} = F(\mathbf{X},t)$. For the first applications of the algorithm, we will focus on families that have a mirror configuration with respect to the $x$-axis, and we are using a corrector algorithm based on a first-order Taylor series expansion of the periodicity conditions. The corrector algorithm uses the \gls{STM}, $\bm{\Phi}$, which maps changes in the initial conditions to changes in the state vector at some short time $t$ later:
\begin{equation}
	\bm{\Phi}(t,t_0) = \pdv{X(t)}{X(t_0)} \, .
	\label{EQ_singshooting_stm}
\end{equation}

To propagate the \gls{STM}, we use the Jacobian Matrix $A(t)$:
\begin{equation}
	\dot{\bm{\Phi}}(t,t_0) = A(t)\bm{\Phi}(t,t_0) \qquad \text{and} \qquad A(t) = \pdv{F(\mathbf{X},t)}{\mathbf{X}}\, ,
	\label{EQ_singshooting_stmpropagated}
\end{equation} 
The general algorithm\cite{Koon2011} constrains the initial and final positions and propagates the state vector from $t_0$ to $t_f$. At $t_f$ we obtain the deviation from the actual position to the desired final state $\delta \mathbf{X}_f$. The left-hand side of Eq.~\eqref{EQ_singshooting_final} shows the deviation in the initial state $\delta \mathbf{X}_0$ required to reach the target state $\mathbf{X}_f$. By constraining the change in initial position to zero, the change in initial velocity as a fucntion of the deviation of the final position can be found (shown in the right-hand side of Eq.~\eqref{EQ_singshooting_final}).
	\begin{equation}
		\delta \mathbf{X}_f = \bm{\Phi}(t_f, t_0) \delta \mathbf{X}_0 \quad ; \quad \delta \mathbf{v}_0 = \bm{\Phi}_{rv}(t_f, t_0)^{-1} \delta \mathbf{r}_f \, .
		\label{EQ_singshooting_final}
	\end{equation} 

The Differential Correction algorithm is an iterative scheme with a high convergence rate if the initial guess is relatively close to the solution. Initial guesses used are obtained from previous authors' studies and literature.\cite{Leiva2006} 

\subsubsection{Numerical Continuation for Families Generation}

We combine the Differential Correction algorithm with a First Order Predictor-Corrector Algorithm (Numerical Continuation scheme) to generate periodic orbit families. This algorithm automatically tracks the family parameter with a simple criterion.\cite{Robin1980, GutierrezRamon2020a} In the $x$-axis mirror configuration, the state vector has the form $\mathbf{x} = (x^1_{01}, 0, 0, x^1_{04})$, with $x_2$ and $x_3$ being the periodicity conditions and time $t^1$. If we define $(x^2_{01}, x^2_{04};t^2)$ as the corresponding parameters of the next orbit of the same family in the neighborhood of the known orbit, such that the quantities
\begin{equation}
	\begin{aligned}
		\Delta x_{01} &= x^2_{01} - x^1_{01} \, , \\
		\Delta x_{04} &= x^2_{04} - x^1_{04} \, ,\\
		\Delta t &= t^2 - t^1 \, ,\\
	\end{aligned}
	\label{EQ_deltasPredictor}
\end{equation}
are small. Substituting these into the periodicity conditions for the second orbit and expanding in Taylor series to first order in the $\Delta$'s we obtain the basic form of the linear predictor algorithm:
\begin{equation}
	\begin{aligned}
		v_{21}\Delta x_{01} + v_{24}\Delta x_{04} + f_2 \Delta t &= 0 \, ,\\	
		v_{31}\Delta x_{01} + v_{34}\Delta x_{04} + f_3 \Delta t &= 0 \, .\\
	\end{aligned}
	\label{EQ_predictorAlgorithm}
\end{equation} 
The values of the first-order variations $v_{kl}$ and time derivatives $f_k$ appearing as coefficients in the equations are those for the known orbit. The 'family parameter' is chosen by applying a constrain to the one degree of freedom in Eq.~\eqref{EQ_predictorAlgorithm}. We specify a fixed increment in the most rapidly-varying initial condition, ensuring that the extrema along the family branch are avoided. This ensures that the algorithm can automatically track the whole family. The predicted values of the parameters $(x^2_{01}$, $x^2_{0}$ and $t^2)$ are obtained by solving the system of Eq.~\eqref{EQ_deltasPredictor} and Eq.~\eqref{EQ_predictorAlgorithm}. The family parameter is checked every time a new orbit in the family is searched for (continuation part of the algorithm), while it is kept fixed during the differential correction part.


\section{Neural Networks Definitions}

\subsection{Echo State Network}

The \gls{ESN} are a type of reservoir-like \gls{NN}.

\subsection{Long Short-Term Memory Scheme}

The \gls{LSTM} are a type of \gls{RNN}.

\subsection{Hybrid Neural Networks}

Hybrid CR3BP + Reservoir-like, from Pathak 2018?

\section{Neural Networks Training and Validation}

Roughly explain the training procedure and validation results (the graphs with errors and such, for some examples)

\section{NN-based Trajectory Generation Benchmark}

Generate different trajectories in CR3BP, give same initial conditions on NN and see what happens. Train with half of the family, and see if the NN can track the rest of the family? Track with 10 orbits of the family, sparingly chosen, and see if it can fill in the gaps? How well does it work as a normal propagator? As a Differential Corrector? As a Numerical Continuator?

\section{Results and Discussion}
Results should be clear and concise. This should explore the significance of the results of the work, not repeat them. A combined Results and Discussion section is often appropriate. Avoid extensive citations and discussion of published literature.

\section{Conclusions}
The main conclusions of the study may be presented in a short Conclusions section, which may stand alone or form a subsection of a Discussion or Results and Discussion section.

\section*{Acknowledgements} 
The Ministry of Education, Culture, Sports, Science and Technology (MEXT) of the Japanese government supported Roger Gutierrez-Ramon under its program of scholarships for graduate school students.

\section*{Appendix A (Title)}
This section is not numbered.

\section*{Appendix B (Title)}
This section is not numbered.


%\begin{thebibliography}{1}
	\bibliographystyle{AAS_publication}   % Number the references.
	\bibliography{astroMLBib}   % Use references.bib to resolve the labels.
%	\bibitem{1} J. van der Geer, J.A.J. Hanraads, R.A. Lupton, The art of writing a scientific article, J. Sci. Commun. 163 (2010) 51–59. 
%	\bibitem{2} Y.-W. Chang, J.-S. Chern, Ups and Downs of Space Tourism Development in 60 Years from Moon Register to SpaceShipTwo Crash, IAC-15-E4.2.8, 66th International Astronautical Congress, Jerusalem, Israel, 2015, 12 – 16 October. 
%	\bibitem{3} W. Strunk Jr., E.B. White, The Elements of Style, fourth ed., Longman, New York, 2000.
%	\bibitem{4} G.R. Mettam, L.B. Adams, How to prepare an electronic version of your article, in: B.S. Jones, R.Z. Smith (Eds.), Introduction to the Electronic Age, E-Publishing Inc., New York, 2009, pp. 281–304.
%	\bibitem{5} E. Mack, Could Virgin Galactic’s Spaceport America be put up for sale? 23 February 2015, http://www.gizmag.com/spaceport-america-hits-nags/36200/, (accessed 30.04.16).	
	
%\end{thebibliography}

\end{document}