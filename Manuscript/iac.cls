\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{iac}[2014/07/24 International Astronautical Congress conference paper LaTeX class]

\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amstext}
\RequirePackage{amsfonts}

\LoadClass[twocolumn,letter paper,10pt]{article}

\RequirePackage[margin=2.25cm,top=3.35cm,bottom=3.35cm]{geometry} % page layout
\RequirePackage{graphicx}
%\RequirePackage{array}
\RequirePackage{booktabs}
%\RequirePackage{overcite}
\RequirePackage{lastpage}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage[explicit,compact]{titlesec}
\RequirePackage[normalem]{ulem}
\RequirePackage{etoolbox}
\RequirePackage{alphalph}

\renewcommand{\headrulewidth}{0pt}
%% Add section numbers in arabic rather than roman 5%
\renewcommand*\sectionmark[1]{\markright{\thesection.\ #1}}


\newcounter{authcount}
\newcommand{\IACauthor}[3]{%
	\stepcounter{authcount}%
	\csdef{iac@author\theauthcount}{#1}%
	\csdef{iac@affiliation\theauthcount}{#2}%
	\csdef{iac@marker\theauthcount}{#3}}

%--------------

\newcommand*{\fnsymbolsingle}[1]{%
  \ensuremath{%
    \ifcase#1%
    \or a%
    \or b
    \or c
    \or d
    \or e
    \else
      \@ctrerr
    \fi
  }%
}

\newalphalph{\fnsymbolmult}[mult]{\fnsymbolsingle}{}
\renewcommand*{\thefootnote}{%
  \fnsymbolmult{\value{footnote}}%
}

%---------------------

\setlength\parindent{12pt}
\RequirePackage[]{caption}
\captionsetup[figure]{name=Fig.,format=hang,indention=-20pt}
\captionsetup[table]{format=hang,indention=-35pt}

\titlespacing{\section}{0pt}{*2}{*0}%1.5
\titlespacing{\subsection}{0pt}{*2}{*0}%0.5
\titlespacing{\subsubsection}{0pt}{*2}{*0}%0.5
\titleformat{\section}{\normalfont}{\bfseries\thesection.\ }{0.5em}{\textbf{#1}}
\titleformat{\subsection}{\normalfont}{\textit{\thesection.\arabic{subsection}}}{0.5em}{\textit{#1}}
\titleformat{\subsubsection}{\normalfont}{\textit{\thesection.\arabic{subsection}.\arabic{subsubsection}}}{0pt}{\textit{#1}}

\renewcommand{\abstract}[1]{\def\iac@abstract{#1}}
\newcommand{\IACkeywords}[6]{\def\iac@keywords{\textbf{Keywords:} #1, #2, #3, #4, #5, #6}}
\newcommand*{\IACpaperyear}[1]{\def\iac@paperyear{#1}}\IACpaperyear{}
\newcommand*{\IACpapernumber}[1]{\def\iac@papernumber{#1}}\IACpapernumber{}
\newcommand*{\IACconference}[1]{\def\iac@conference{#1}}\IACconference{}
\newcommand*{\IACcopyright}[1]{\def\iac@copyright{#1}}\IACcopyright{}
\newcommand*{\IAClocation}[1]{\def\iac@location{#1}}\IAClocation{}

\newcommand{\IACcopyrightA}[2]{\def\iac@copyright{Copyright \copyright\,#1 by #2. All rights reserved.}}
\newcommand{\IACcopyrightB}[2]{\def\iac@copyright{Copyright \copyright\,#1 by #2. Published by the International Astronautical Federation with permission.}}
\newcommand{\IACcopyrightC}{\def\iac@copyright{This material is declared a work of the U.S.\ Government and is not subject to copyright protection in the United States.}}
\newcommand{\IACcopyrightD}[1]{\def\iac@copyright{Copyright \copyright\,#1 by the International Astronautical Federation. The U.S.\ Government has a royalty-free license to exercise all rights under the copyright claimed herein for Governmental purposes. All other rights are reserved by the copyright owner.}}
\newcommand{\IACcopyrightDAPL}[1]{\def\iac@copyright{Copyright \copyright\,#1 by the International Astronautical Federation. Under the copyright claimed herein, the U.S.\ Government has a royalty-free license to exercise all rights for Governmental purposes. All other rights are reserved by the copyright owner.}} 
\newcommand\iac@makecopyright{{\iac@copyright}}

\renewcommand\maketitle{\twocolumn[
  \begin{@twocolumnfalse}
   \lhead{}\chead{\footnotesize \iac@conference $^{th}$ International Astronautical Congress (IAC), {\iac@location}. \\ \iac@makecopyright}\rhead{}%
  \lfoot{IAC--\iac@paperyear--\iac@papernumber}\cfoot{}\rfoot{Page \thepage\ of \pageref{LastPage}}%
  \begingroup
    \global\@topnum\z@   % Prevents figures from going at top of page.
    \@maketitle
    \@thanks
  \endgroup
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\date\relax
  \global\let\and\relax
  \end{@twocolumnfalse}
  ]}
\def\@maketitle{%
  \newpage
  \begin{center}%
    IAC--\iac@paperyear--\iac@papernumber\par%
    \vskip1em
    { \textbf{\@title} \par}%
    \vskip 1.5em%
\end{center}
   
      \lineskip .5em%
\begin{center}
      \newcounter{authnum}%
      \setcounter{authnum}{0}
      \whileboolexpr
      { test {\ifnumcomp{\value{authnum}}{<}{\theauthcount}} }%
      {\stepcounter{authnum}%
      	\normalsize\textbf{\csuse{iac@author\theauthnum}\footnotemark \csuse{iac@marker\theauthnum} }}%
\vskip 1.5ex%
  \end{center}%

 \setcounter{authnum}{0}
      \whileboolexpr
      { test {\ifnumcomp{\value{authnum}}{<}{\theauthcount}} }%
      {\stepcounter{authnum}%
      	\normalsize\footnotemark[\value{authnum}]\csuse{iac@affiliation\theauthnum}\par%
		
	
    }%
\normalsize{$^*$Corresponding Author }
\vskip 1.5ex%

\begin{center}
\textbf{Abstract}
  \end{center}%
  \hspace{12pt}\indent\iac@abstract\par%
  \indent\iac@keywords\par%
  \vskip 4.5ex}%
  \pagestyle{fancy}%
 % ----------------------------

\endinput